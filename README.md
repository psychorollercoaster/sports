Help disadvantaged youngsters play cricket world cup

One of those programmes is Sports Kit Recycling, which collects usable kit from across the UK which is then sorted and redistributed to partner organisations around the world who can put it to great use in their communities. Last year, we donated more than 12,000 items of kit to projects in 21 different countries… and we need your help to continue to support projects in the UK and across the world to access the right kit and equipment to help us deliver thousands of opportunities for young people to play sport. One project we are supporting in 2019 is the East African Character Development Trust in Kenya who work with 17 schools from the slums of Nairobi and beyond, delivering PE lessons to nearly 4,500 boys and girls every week.

Many of the children are orphans or from single-parent families from some of the poorest communities in Kenya and they have no sporting equipment or clothing – and we need your help to give these youngsters the opportunity to play sport.

How You Can Help?
To help us meet the request from Kenya, we need your help collecting junior kit and equipment including:
• Junior / youth clothing
• Junior / youth bats, gloves and pads.
• Protective equipment including helmets, arm/thigh guards, abdominal guards
• Junior-size cricket balls
• Cricket shoes
• Stumps
• Kwik Cricket equipment

While we are looking for the above to meet the request from the East African Character Development Trust, any sports kit – junior or adult – that you can donate will be gratefully received. Also, in order to receive more kit, we need your cricket club, school, shop, offce or even your home to become a kit recycling ‘collection hub.’ Don’t worry, you won’t be inundated with sports kit every day! Most of our hubs are set up to receive kit at particular times of year, normally just before the UK cricket season starts and shortly after it finishes.

Billings and Malcolm join Luton Wicketz youngsters
The Lord’s Taverners has been announced as an official ‘Cricket 4 Good Partner’ of the World Cup. To launch the partnership, the Lord’s Taverners Luton Wicketz project hosted nearly 100 youngsters from 20-plus different nationalities to take part in a mini-world cup tournament at Putteridge High School, which included a visit from the ICC Cricket World Cup Trophy and coaching tips from England star and Lord’s Taverners ambassador Sam Billings and ex-England paceman Devon Malcolm. There will be more visits to Lord’s Taverners programmes as part of the ICC Cricket World Cup Trophy Tour, driven by Nissan. In addition, young leaders from the Lord’s Taverners programmes will be given the chance to step on to the fi eld of play as official ICC Men’s Cricket World Cup ‘Flag Bearers’ and play a role as tournament volunteers – known as Cricketeers. The Lord’s Taverners is the leading youth cricket and disability sports charity in the UK, delivering a number of national programmes, including Table Cricket, Super 1s and Wicketz, which improve the prospects of thousands of disadvantaged and disabled young people through sport and recreation. This partnership will give participants of these programmes the unique opportunity to play an active role in the upcoming World Cup.

https://iplnewslatest.blogspot.com/2019/03/icc-cricket-world-cup-2019-Schedule.html